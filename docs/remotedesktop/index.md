---
title: "多个远程桌面的集群管理方案。"
date: 2022-02-26T14:53:55+08:00
draft: false
slug: Remotedesktop
---

最近获取了好几台Windows远程桌面。虽然基本功能使用Windows自带的管理系统也就足够了，但是对于多台机器，用自带的设备切换起来就非常麻烦。

## 远程桌面（微软官方版，在微软商店直接下载）

这是微软官方推出的一款功能稍有增强的远程桌面软件，支持同时管理、实时监控多台设备，支持导出、导入（但是不含密码），支持窗口化。界面较为美观。

直接在微软商店搜索“远程桌面”即可。

![](2022-02-27-10-10-38.png)

## PRemoteM

PRemoteM 是一款现代的远程会话管理和启动器，它让你能够在任何时候快速开启一个远程会话。目前 PRemoteM 已支持 微软远程桌面(RDP)、VNC、SSH、Telnet、SFTP, FTP, RemoteApp等协议。

界面较为美观，但是不支持导出配置。不太方便。

![](2022-02-27-10-15-33.png)

[更多演示与详细介绍](https://github.com/VShawn/PRemoteM/wiki/Intro-%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)

[GitHub开源软件，项目地址](https://github.com/VShawn/PRemoteM)

[下载链接](https://github.com/VShawn/PRemoteM/releases/download/0.6.1.1/PRemoteM-0.6.1.1.zip)

可以参考[本文](https://www.cnblogs.com/singlex/p/HowToManageYourRdpSession.html)使用。

## mRemoteNG

未能配置成功。长期未更新

GitHub开源软件，项目地址


参考：

1. https://blog.csdn.net/qq_23009105/article/details/82865099
2. https://www.cnblogs.com/singlex/p/HowToManageYourRdpSession.html