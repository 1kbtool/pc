---
title: "MS Office 安装和激活最全指南"
slug: Office
date: 2022-05-23T18:10:27+08:00
draft: false
---

由于 Office 相关内容较多，较复杂。这里分为多节进行阐述。

## 简介

Office 是微软公司研发的办公套件，包括 Word、Excel、PPT 等。

- 经过多年的迭代，目前买断制（终身制） Office 最新版本为 office 2021。另有订阅制（按年购买）的 office 365。此外，注册微软账户后可以直接使用免费的 Office Online（网页版）。

- 近年来微软也在移动平台上开发了对应的 Office 应用。您可以免费下载。但高级功能需要 Office 365 订阅。个人感觉 Office 在移动端的适配并不好。



### 相关链接

买断制 office 2021 购买链接：[Microsoft Office官网下载-正版微软Office2021办公软件-微软官方商城](https://www.microsoftstore.com.cn/software/office)

订阅制 office 365 链接：[Microsoft 365官网, Office, 原Office 365办公软件下载-Microsoft 365](https://www.microsoft.com/zh-cn/microsoft-365)

Office 官方网页版（免费服务）：[Office 365 Login | Microsoft Office](https://www.office.com/)


Office Tool Plus：[Office Tool Plus 官方网站 - 一键部署 Office](https://otp.landian.vip/zh-cn/)


## 使用教程

### office 2021 和 office 365 区别

office 2021 一次买断，只能用于一台设备。终身使用，但不支持后续更新，并且价格较高。

一般我们说 office 2021，只涉及 Word、Excel、PPT 三剑客。

>可以免费使用KMS激活。

office 365 按年购买，一个订阅可以用于多台设备，支持自动更新。较便宜（相对）。

除基础软件外，office 365 还包括 Onedrive 网盘、Onenote 笔记、Teams 团队协作等。

>可以免费申请 E5 订阅。

个人用 订阅制 office 365 较多。

### office 包含的软件

许多人对 office 的理解停留在 Word、Excel、PPT 三剑客的阶段，实际上 Office 专业版本还包含 Publisher、Access 等工具。不过这些我们都用的很少，却又占用大量空间，很不划算。

官网下载的安装工具会自动依照您的订阅安装所有软件，例如专业版/企业版则会直接安装 Word、Excel、PPT+Publisher、Access+Outlook+Onenote。。。为了实现自定义的安装和管理，我们可以使用 Office Tool Plus 这款软件。

### Visio

Visio 不包含在 Office 套件中，但可作为独立应用程序出售。 

[安装 Visio 或访问 Visio 网页版](https://support.microsoft.com/zh-cn/office/%E5%AE%89%E8%A3%85-visio-%E6%88%96%E8%AE%BF%E9%97%AE-visio-%E7%BD%91%E9%A1%B5%E7%89%88-f98f21e3-aa02-4827-9167-ddab5b025710#VisioInstall=Office_2013_-_2021)



## 小编吐槽

@stark

stark 的想法

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

