---
title: "正版 Office 快速安装教程"
slug: Office Tutorial
date: 2022-05-24T12:20:17+08:00
draft: false
---

## 简介

此文致力于以最快捷的方式，以正版渠道安装/激活您的 Office 软件。**基于 开发人员 E5 订阅。**

### 相关链接


## 使用教程

### 安装 Office

>已经安装 Word、Excel、PPT 本体软件的可以跳过这一步，Windows 自带的 “Office” 不算（实际上这只是一个下载器/启动器）。

[安装工具下载](https://otp.landian.vip/redirect/download.php?type=runtime&site=sdumirror)

打开后点击 部署，如图选择：

![](2022-05-24-12-32-51.png)

等待安装完成即可。不用着急打开，我们弄个正版 Office 365 账号先。

### 激活 Office

按照本文

## 小编吐槽

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

