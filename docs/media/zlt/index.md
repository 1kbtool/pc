---
title: "ZonyLrcToolX：歌词自动匹配下载神器"
date: 2022-02-05T22:48:28+08:00
draft: false
slug: Zlt
categories: ["影音娱乐"]
tags: ["音乐","Github"]
---

## 简介

某些音乐软件在下载歌曲的时候不支持同步下载歌词，如果转存到其他设备、MP3上播放，就会因为没有歌词使得体验大打折扣。但是一首一首地下载歌词、匹配文件名又显得过于麻烦。

ZonyLrcToolX就是这样一款能够批量下载本地歌曲的匹配lrc格式歌词文件的免费开源软件。

[项目地址](https://github.com/real-zony/ZonyLrcToolsX)

[旧版项目地址](https://gitee.com/jokers/ZonyLrcTools)

![](2022-02-05-22-58-30.png)

## 使用说明

最近作者在开发新版本，但是新版图形界面还没做出来，只能用命令行控制使用。有点不人性化。我们就先用旧版。

[旧版下载地址](http://myzony.com/usr/uploads/ZonyLrc/ZonyLrcTools%200.0.3.0.zip)

[备用下载地址](https://url19.ctfile.com/f/35490519-541843268-ef48f5)（访问密码：3202）

## 备注

网上似乎有新版了：

[似乎是新版的下载地址](https://www.mediafire.com/file/val0taljpddve9v/ZonyLrcToolsX.exe/file)

![](2022-02-05-23-01-11.png)

参考：

1. https://zhuanlan.zhihu.com/p/54733963