---
title: "musicbrainz picard：音乐元数据获取工具"
date: 2022-02-16T15:14:16+08:00
draft: false
slug: Musicbrainz
---
### 介绍

可以用来识别一些完全缺少tag信息的音乐。应该是直接通过数据情况来计算的。

### 下载

[官网][1]

### 使用

添加文件夹、全选、扫描、保存
[![4NCHz9.png](https://z3.ax1x.com/2021/09/22/4NCHz9.png)](https://imgtu.com/i/4NCHz9)

  [1]: https://picard.musicbrainz.org/
参考：
