---
title: "Yesplaymusic：强大、简洁网易云桌面第三方客户端，解锁灰色歌曲"
date: 2022-03-04T21:39:41+08:00
draft: false
slug: Yesplaymusic
---

Yesplaymusic是一款高颜值的第三方网易云播放器，支持 Windows / macOS / Linux。

自带Unlock，**解锁灰色歌曲**。

>注意！使用中发现本项目不太稳定。推荐切换到洛雪音乐助手。

[项目地址](https://github.com/qier222/YesPlayMusic)

![](2022-03-09-08-29-13.png)

![](2022-03-09-08-29-51.png)

参考：
