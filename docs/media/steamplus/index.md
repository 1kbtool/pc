---
title: "Steam辅助工具合辑：历史价格查看、多用户切换、加速访问"
date: 2022-02-11T11:06:52+08:00
draft: false
slug: Steamplus
---


- [SteamDB — a database of everything on Steam](https://steamdb.info/)

可以查看游戏在线人数，最近的火热程度，以及最重要的：**各地区的历史价格**

![image-20211117133408772](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211117133408772.png)

搜索只支持英文

![image-20211117133449867](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211117133449867.png)

点击`Game`的`APPID`即可查看

![image-20211117133536171](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211117133536171.png)

### steam++|多账号登录、加速访问

- [Steam++ - 主页 (steampp.net)](https://steampp.net/)

![image-20211117133919009](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211117133919009.png)

![image-20211117134024370](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211117134024370.png)

实际使用：只能说挺一般，账号可能不太安全。功能，也不是很多。

不过有些木得代理的朋友可以试下这个加速访问。

参考：
