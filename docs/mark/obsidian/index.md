---
title: "【Mark】obsidian你的第二个大脑，打造你的知识体系。"
date: 2022-03-03T22:48:48+08:00
draft: false
slug: Obsidian
categories: ["Mark"]
tags: ["笔记软件","Mark"]
---

Mark原因：暂时不需要，Onenote够了，并且手写功能对我比较重要

https://sspai.com/post/71405
obsidian你的第二个大脑，打造你的知识体系。

obsidian官网：https://obsidian.md/
主题：https://github.com/kmaasrud/awesome-obsidian
中文手册：https://github.com/Jackiexiao/obsidian
相关：
中国第一个公开的卢曼笔记 (https://www.coachwugang.com/_Homepage)   一个内容丰富学习笔记，如果想构造属于一个自己的知识体系，不妨看看这个样式。

用 Git 在 Android 和 Windows 间同步 Obsidian 数据库 - 少数派 (https://sspai.com/post/68989)  个人比较推荐的笔记同步方法，但是有一定的门槛。
#笔记 #obsidian #少数派

[官网](https://obsidian.md/)

![](2022-03-03-22-49-11.png)

https://zhuanlan.zhihu.com/p/349638221

参考：


