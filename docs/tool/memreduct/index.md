---
title: "Memreduct：一款小巧的内存清理软件"
date: 2022-01-30T23:23:30+08:00
draft: false
slug: memreduct
categories: ["实用工具"]
tags: ["系统工具"]
---

## 简介

Mem Reduct是一个大小仅有400KB的，无广告且免费的，可以实现一键清理电脑内存的小软件。

[官网](https://www.henrypp.org/product/memreduct)

[下载地址](https://github.com/henrypp/memreduct/releases/download/v.3.4/memreduct-3.4-setup.exe)

[备用下载地址](https://url19.ctfile.com/f/35490519-540906784-b070f8)（访问密码：3202）

![](2022-01-31-11-13-29.png)

![](2022-01-31-11-13-36.png)

## 使用

软件支持显示中文。如果你安装好了之后发现软件显示英文的话，依次点击【Setting】，【Language】，【Chinese(Simplified）】就可以让它显示中文简体：

![](2022-01-31-11-13-51.png)

软件使用方法非常简单：打开软件，点击【清理内存】就能清理电脑内存：

![](2022-01-31-11-15-32.png)

点击【文件】，点击【设置】，点击【内存清理】还可以把它设置成自动清理内存。触发器中的【清理当超过__时】表示当内存占用率超过某个值时自动清理内存。【清理每__分钟】表示每XX分钟自动清理一次内存。所以呢，触发器大家可以根据自己的需要去设置：

![](2022-01-31-11-14-41.png)

参考：

1. https://zhuanlan.zhihu.com/p/432214539
2. https://zhuanlan.zhihu.com/p/345208266