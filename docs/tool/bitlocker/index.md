---
title: "Bitlocker：给U盘、主板加上密码"
date: 2022-02-16T16:04:28+08:00
draft: false
slug: Bitlocker
categories: ["实用工具"]
tags: ["系统维护"]
---

> 之前写了几篇关于PE系统的文章，相信大家都已经发现了，利用PE系统，哪怕是电脑小白也可以在很短的时间内绕过开机密码，读取电脑硬盘数据。
>
> 其实家用电脑的保护确实是比较低的，设想一种最暴力的情况：如果有人把你的硬盘卸下来，直接读取里面的信息，又该怎么办呢？

本文分为两个部分，第一部分简要介绍如何给硬盘进行加密，第二部分介绍如何给BIOS设置密码（阻止PE系统）。

**注意：除非有必要，请尽量不要对常用的设备设置复杂的密码，否则忘记密码导致数据无法访问等风险，自行承担！**

### 使用Bitlocker给硬盘进行加密

> **BitLocker**（直译为“[比特](https://zh.wikipedia.org/wiki/位元)锁”）是内置于[Windows Vista](https://zh.wikipedia.org/wiki/Windows_Vista)及其之后系统的全[磁盘加密](https://zh.wikipedia.org/wiki/磁盘加密)功能，透过为整个[卷](https://zh.wikipedia.org/w/index.php?title=磁碟區_(计算)&action=edit&redlink=1)提供[加密](https://zh.wikipedia.org/wiki/加密)来保护数据。它默认在[密码块链接](https://zh.wikipedia.org/wiki/分组密码工作模式#密码块链接（CBC）)（CBC）或[XTS](https://zh.wikipedia.org/w/index.php?title=磁盘加密理论&action=edit&redlink=1)模式下使用128位或256位[密钥](https://zh.wikipedia.org/wiki/密钥)的[AES](https://zh.wikipedia.org/wiki/高级加密标准)加密算法[[1\]](https://zh.wikipedia.org/wiki/BitLocker#cite_note-:0-1)[[2\]](https://zh.wikipedia.org/wiki/BitLocker#cite_note-2)[[3\]](https://zh.wikipedia.org/wiki/BitLocker#cite_note-3)。其中CBC用于每个单独的[磁盘扇区](https://zh.wikipedia.org/wiki/磁盘扇区)，不在整个磁盘上使用[[4\]](https://zh.wikipedia.org/wiki/BitLocker#cite_note-4)。
>
> ——摘自维基百科

利用Bitlocker，可以对U盘、移动硬盘、电脑硬盘等进行加密。一旦加密，**必须在Windows系统下输入密码才能访问数据**，或者利用恢复文件进行恢复。

**请注意，如果你同时忘记了密码和丢失了恢复文件，那么很不幸，你的数据可能就没有任何手段可以恢复了**

下面简单介绍一下如何给U盘上BitLocker

![image-20211118232748272](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211118232748272.png)

**设置一个密码，不用太复杂，主要是保证自己能够记得住**

![image-20211118232909643](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211118232909643.png)

选择你的恢复文件保存位置（这也很重要，假如你不幸忘了密码），我这里保存到微软账户：

![image-20211118232959958](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211118232959958.png)

等待保存完成，单击下一页。

之后会显示选择对整个驱动器进行加密还是仅加密已用空间，**前者甚至可以保护已经被删除了的文件的残留，但是用时较长。**以及选择加密方式。按个人情况进行选择即可：

![image-20211118233249409](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211118233249409.png)

![image-20211118233253422](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211118233253422.png)

**开始加密！**

![image-20211118233315329](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211118233315329.png)

等待加密完成。下次再插入这个U盘时，就需要输入密码才能查看了：
![image-20211118234519974](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211118234519974.png)

### 给BIOS设置密码

关于如何进入BIOS可以参考之前的文章。

以华为笔记本为例，进入BIOS后很容易看到“配置管理员密码”和“开机密码”选项。前者是修改BIOS选项时进行身份验证，后者是开机进入BIOS时验证（关于正常开机会不会验证，不同设备可能不一样）。

**注意：一定要设置一个自己记得住的密码，密码一旦设置不可撤销，谨慎使用**

![20211118233636](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/20211118233636.jpg)

参考：
