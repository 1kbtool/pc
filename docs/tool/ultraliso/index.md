---
title: "ISO刻录至U盘工具（PE、启动盘制作）Ultroiso和refus"
date: 2022-02-16T15:11:30+08:00
draft: false
slug: Ultraliso
---

### 简介

UltraISO是一款ISO刻录至U盘工具，一般用来做U盘启动等，适用于windows、ubuntu。。。

### 下载地址

[点我][1]

### 简易刻录教程

选择ISO文件
[![41x7sH.png](https://z3.ax1x.com/2021/09/19/41x7sH.png)](https://imgtu.com/i/41x7sH)

选择刻录到硬盘
[![41xqeA.png](https://z3.ax1x.com/2021/09/19/41xqeA.png)](https://imgtu.com/i/41xqeA)

确认ISO文件和目标U盘之后开始刻录，注意会清除U盘所有文件
[![41zpQg.png](https://z3.ax1x.com/2021/09/19/41zpQg.png)](https://imgtu.com/i/41zpQg)


  [1]: https://sharelist-kerm.herokuapp.com/share/soft/green/UltraISO.exe

参考：
