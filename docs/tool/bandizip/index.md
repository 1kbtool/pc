---
title: "Bandizip（中文旧版）：免费、无广告、好用的Windows平台压缩软件"
date: 2022-02-03T18:11:38+08:00
draft: false
slug: Bandizip
---

## 简介

Bandizip 是一个强大的多功能压缩文件管理工具，可提供很快的解压缩速度和其他各种实用功能。

[官网](http://www.bandisoft.com/bandizip/?utm_source=iplaysoft.com&hmsr=iplaysoft.com)

界面简洁，没有烦人的弹窗广告（旧版），免费，安全。功能该有的都有，还支持一键将压缩包内文件拖动到目标文件夹。

![](2022-02-04-19-14-47.png)

新版本有广告并且免费功能弱化，故推荐使用旧版。注意安装后将自动更新关闭。

![](2022-02-04-19-26-29.png)

[旧版下载链接](https://www.lanzoui.com/i8av00j)

[备用下载链接](https://url19.ctfile.com/f/35490519-541621377-5544c8)（访问密码：3202）

参考：

1. https://www.iplaysoft.com/bandizip.html