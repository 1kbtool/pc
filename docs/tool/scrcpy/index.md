## 简介

scrcpy是一款轻量、开源、免费、强大、配置简单的电脑控制手机软件。

只需使用数据线连接电脑和手机、打开调试，即可方便快捷地实时显示手机画面、用鼠标键盘操控手机。

[项目地址](https://github.com/Genymobile/scrcpy)

[下载链接](https://github.com/Genymobile/scrcpy/releases/download/v1.23/scrcpy-win64-v1.23.zip)

[备用下载连接](http://tmp.link/f/622ee5d34e391)

![效果](2022-03-14-14-40-22.png)

## 使用教程

使用数据线连接电脑和手机（也可以无线连接，参考[官方手册](https://github.com/Genymobile/scrcpy/blob/master/README.zh-Hans.md)）

打开开发人员模式，一般是在“关于手机”页面，连击“版本号”数次。

![](2022-03-14-14-34-49.png)

开启开发人员选项-USB调试。

![](2022-03-14-14-33-34.png)

将手机与电脑连接，在弹出的窗口中，选择允许调试：

![](2022-03-14-14-36-05.png)

下载、解压软件。转到解压后的软件文件夹。

![](2022-03-14-14-36-45.png)

右键（Windows10按下shift再右键），选择在Windows终端中打开（Windows10是打开命令窗口）

### 控制手机

依次输入以下命令：

```
# 切换到命令提示符
cmd
# 连接adb调试
adb usb
# 打开主程序
scrcpy.exe
```

就会自动弹出手机界面了。

更详细的操作可以参考[官方手册](https://github.com/Genymobile/scrcpy/blob/master/README.zh-Hans.md)

![](2022-03-14-14-40-43.png)

### 后台录屏

可以在镜像的同时录制视频：

```
scrcpy --record file.mp4
```

更详细的操作可以参考[官方手册](https://github.com/Genymobile/scrcpy/blob/master/README.zh-Hans.md)

参考：

1. https://blog.csdn.net/weixin_45460120/article/details/110533253