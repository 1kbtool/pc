---
title: "使用开源免费工具 LaTeX-OCR 识别数学公式"
description: "使用开源免费工具 LaTeX-OCR 识别数学公式"
slug: 使用开源免费工具 LaTeX-OCR 识别数学公式
date: 2022-07-26T16:32:16+08:00
draft: false
tags: ['']
categories: ['']
---

本文基于：https://www.zhihu.com/question/338462426/answer/2412271227 完善发布。


## 简介

LaTeX-OCR 是一个基于机器学习的开源免费的公式识别软件，和 Mathpix 的功能十分相似，不过是基于本地程序且完全免费。

![](./2022-07-26-19-08-07.png)

### 相关链接

- 官方地址：https://github.com/lukas-blecher/LaTeX-OCR/

## 使用教程

您如果能够直接访问 Github，并有一定编程基础。可以直接按照 Readme 指示做。这个教程是为一般用户准备的。

首先请确保已经安装 Python。

win 键 + R，打开“运行”窗口，然后输入 powershell 回车，打开命令行。

安装 Pytorch

```bash
pip3 install torch torchvision torchaudio
```

如果下载缓慢可以使用清华源，以提升国内下载速度，加上一行即可，例如：

```bash
pip3 install torch torchvision torchaudio -i https://pypi.tuna.tsinghua.edu.cn/simple some-package
```

下同。

安装软件本体

```
pip3 install pix2tex[gui] -i https://pypi.tuna.tsinghua.edu.cn/simple some-package
```

由于需要在本地运行图像识别算法，下载的软件包会比较大，请耐心等候。

直接运行 `latexocr`，等待模型文档下载完成（对，还要下模型）后，如果一切正常（大概率不正常，参考下节错误排除）会弹出界面窗口，点击 `Snip` 截图选框识别即可。

![](./2022-07-26-19-14-57.png)

之后运行只要打开 powershell 输入 `latexocr` 即可马上启动。

### 错误排除

**问题1** 运行 `latexocr` 后，模型文档下载很慢，需要数个小时，显示如下。

```
download weights v0.0.1 to path C:\Users\admin\AppData\Local\Packages\PythonSoftwareFoundation.Python.3.10_qbz5n2kfra8p0\LocalCache\local-packages\Python310\site-packages\pix2tex\model\checkpoints
weights.pth:  29%|█████████████████▉                                            | 28.1M/97.4M [35:35<1:27:33, 13.8kb/s]
```

直接从镜像站[下载模型文件](https://proxy.cnhub.dev/https://github.com/lukas-blecher/LaTeX-OCR/releases/download/v0.0.1/weights.pth)

然后放到 `\pix2tex\model\checkpoints` 目录中。例如，参考上上行，如果是这种输出，说明我应该将这个文件放到（第一行 path 以后全部复制）

```
C:\Users\admin\AppData\Local\Packages\PythonSoftwareFoundation.Python.3.10_qbz5n2kfra8p0\LocalCache\local-packages\Python310\site-packages\pix2tex\model\checkpoints
```

中。再次运行即可。

**问题2** 可以打开界面，但是总是识别失败，运行出现 Numpy fail 等。

numpy版本和pytorch所需的numpy版本不匹配

```
pip install numpy --upgrade
```



## 说明

此软件用于划取单一公式识别非常方便，若是识别一整篇 PDF 文档中公式，**推荐使用百度 OCR API**，支持文字公式混合识别，支持文档识别。

参考：https://github.com/kerms5/baiduocrtool

## 附录

### Reference

- [如何看待 Mathpix 收费？ - 知乎](https://www.zhihu.com/question/338462426/answer/2412271227)
- [解决 RuntimeError: module compiled against API version 0xf but this version of numpy is 0xd_gcygeeker的博客-CSDN博客](https://blog.csdn.net/gongchenyu/article/details/123498648)
- [N博客](https://blog.csdn.net/XBZOliver/article/details/124456290)
- [PyTorch](https://pytorch.org/get-started/locally/)
- [GitHub - lukas-blecher/LaTeX-OCR: pix2tex: Using a ViT to convert images of equations into LaTeX code.](https://github.com/lukas-blecher/LaTeX-OCR/)

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

