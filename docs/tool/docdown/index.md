---
title: "Docdown：开源免费的 原创力文档 & 豆丁网 & 百度文库 下载工具 "
description: "Docdown：开源免费的 原创力文档 & 豆丁网 & 百度文库 下载工具 "
slug: Docdown
date: 2022-07-22T12:01:00+08:00
draft: false
tags: ['']
categories: ['']
---

## 简介

docdown 是一款使用 playwright 强力驱动的 原创力文档 book118 & 豆丁网 docin & 百度文库 baiduwenku 下载工具。

支持范围：book118 doc ppt pdf，docin doc，百度文库。

![exmaple.gif](https://s2.loli.net/2022/07/18/QjWychz39RHZuUe.gif)

### 相关链接

- 下载：[docdown](https://proxy.cnhub.dev/https://github.com/kerms5/docdown/releases/download/v1.0.0/docdown.exe)
- 官网：[DocDown 使用 Playwright 驱动的 豆丁 docin / 原创力文档 book118/ 百度文库 baidu 预览文档下载工具](https://docdown.net/)

## 使用教程

访问待下载网站，点击预览，复制链接，格式如下；

```
https://max.book118.com/html/2017/1105/139064432.shtm
```

以上面的链接为例，在下载目标文件夹下，右键-在终端中打开（Windows11），按住 Shift+右键-在此处打开 Powershell 窗口（Windows10），然后输入以下内容并回车：

```Powershell
./docdown 下载链接带英文引号

# 例如：
./docdown 'https://max.book118.com/html/2017/1105/139064432.shtm'
```

之后会弹出浏览器窗口，一段时间后会在目录下生成 PDF 文件。

### 使用问题

如果遇到运行错误请先确保以下内容均已注意，再提 issue。

- 注意关闭系统代理。
- 复制粘贴链接时需要打上英文引号`'`。

### 技术问题

目前这些问题无法解决，如果您有好的解决方法请提 issue。

- 部分文档格式不支持。
- 需要付费预览的文档不支持。
- 只支持下载为 PDF 格式（image 转 pdf）。
- 百度文库清晰度较低（Playwright 截图限制）。


## 小编吐槽

@royse

NB！但是百度文库清晰度有点低，容易失败。

## 附录

### Reference

- [DocDown 使用 Playwright 驱动的 豆丁 docin / 原创力文档 book118/ 百度文库 baidu 预览文档下载工具](https://docdown.net/)

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

