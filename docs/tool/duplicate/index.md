---
title: "czkawka：免费重复文件、相似图片清理工具"
date: 2022-02-09T20:45:35+08:00
draft: false
slug: duplicate
---

一款免费的重复文件清理工具，功能强大。在GitHub上开源，没有中文，但是界面很简单，参考本文使用起来应该不难。

- [项目地址](https://github.com/qarmin/czkawka)
- [下载地址](https://github.com/qarmin/czkawka/releases/download/3.2.0/windows_czkawka_gui.zip)
- [备用下载地址](https://kerms-sharelist.herokuapp.com/%5B%E8%BD%AF%E4%BB%B6%5Dsoft/%5B%E6%96%87%E4%BB%B6%E7%AE%A1%E7%90%86%5DFileManager/windows_czkawka_gui.zip)
- [备用下载地址2](http://tmp.link/f/6203aaa934b5d)

![image-20211110180705714](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211110180705714.png)

## 使用教程

如图，该有的都有，就是界面有点丑陋：

![image-20211110180926710](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211110180926710.png)

先按左上角Add添加文件夹（Remove删除），然后左下角Search自动扫描。完成之后按左边栏每一项都会显示对应的结果。比如我这里选的Duplicate Files，则显示了所有重复文件。

![image-20211111190334787](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211111190334787.png)

按select选择之后，按delete就可以删除了。

这里顺便说一下这个select custom的用法：

![image-20211111190458851](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211111190458851.png)

比如你要删除所有在 新建文件夹 里面的，那么可以输入`*/新建文件夹*/*`就会自动选择新建文件夹里面的文件了。