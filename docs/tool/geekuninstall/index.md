---
title: "Geekuninstaller：小巧而强大的卸载工具，可卸载UWP应用、360等。"
date: 2022-01-31T11:52:55+08:00
draft: false
slug: Geekuninstall
categories: ["实用工具"]
tags: ["系统工具"]
---

## 重要说明

请不要用GeekUninstaller卸载**无法确定它是做什么用**的软件，以免误删系统重要组件引起崩溃。

## 简介

Geek Uninstaller是一款用于Windows平台的比较小巧暴力的卸载软件。

[官网](https://geekuninstaller.com/)

[备用下载地址](https://url19.ctfile.com/f/35490519-540928703-c0bb6d) （访问密码：3202）

简单在官网下载Free版本的zip压缩包即可，只有2.52MB，解压后也只有1个exe文件，大小6MB，非常小巧，并且即便是把它当做一个快捷方式放在桌面上来用，也不会产生其他的临时文件或软件日志。

![](2022-01-31-12-00-57.png)

## 使用

在想卸载的软件右键，弹出卸载小窗口。（或者双击直接卸载）。

Geek Uninstall会监控上一次打开软件到现在新装的各种软件，并黄色高亮标注。哪些是你自己安装的，哪些是偷偷下载安装的一目了然。

Geek Uninstall不仅能卸载Windows桌面程序，还可以卸载Windows 商店应用（UWP应用），需要在 查看 打开设置。。

![](2022-01-31-12-06-14.png)

卸载会调用程序自带的卸载器，卸载后还会自动扫描残留痕迹，包括注册表等，可以继续进行删除。

![](2022-01-31-12-06-47.png)

参考：

1. https://its401.com/article/nbk2014/114602063
2. https://www.bilibili.com/read/cv8641037/
