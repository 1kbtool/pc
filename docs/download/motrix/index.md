---
title: "Motrix：强大、无广告的全能下载工具，支持Windows、MacOS、Linux"
date: 2022-02-11T10:27:00+08:00
draft: false
slug: Motrix
categories: ["下载工具"]
tags: ["Aria2","BT下载"]
---

## 简介

Motrix 是一款开源、免费的全能下载工具，支持下载 HTTP、FTP、BT、磁力链等资源。基于electron和aria2开发。

除了电驴（ed2k）、迅雷格式之外，其他格式，包括BT都可下载。支持多线程高速下载。**完全可以替代迅雷，并且没有广告。**

- 官网：[Motrix](https://motrix.app/zh-CN)

![image-20211113123545503](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211113123545503.png)

[项目地址](https://github.com/agalwood/Motrix)

[下载地址](https://github.com/agalwood/Motrix/releases/download/v1.6.11/Motrix-1.6.11.exe)

[备用下载地址](http://tmp.link/f/6205cb3aa4f91)

- 优点：下载格式非常全，，基本包含所有常用格式，使用aria2内核，下载速度很快。**通过某些插件支持百度云下载。**
- 缺点：aria2内核，本质上还是aria2，electron前端，资源消耗可能比较大，推荐有能力的朋友直接使用aria2+ariaNG或者搭建离线下载服务器。

参考：
