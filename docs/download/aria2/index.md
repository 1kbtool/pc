---
title: "Aria2：开源、免费、跨平台、高速、易拓展的下载工具，支持BT和直链"
date: 2022-02-11T10:19:40+08:00
draft: false
slug: Aria2
categories: ["下载工具"]
tags: ["Aria2","BT下载"]
---

Aria2是一款自由、轻量、跨平台命令行界面的下载管理器。支持的下载协议有：HTTP、HTTPS、FTP、Bittorrent和Metalink。

优势

- 开源，免费，无广告（命令行能有什么广告）
- 大量第三方小白式客户端，开箱即用
- 跨平台，支持 Linux，macOS 和 Windows
- 支持磁力，BT 和 HTTP 下载
- 无侵入式行为，性能强悍，下载速度毫不逊色于迅雷

## Aria2的基本使用

Aria2仅仅是一个内核（命令行程序），直接使用比较困难。基于Aria2开发出了很多第三方工具，开箱即用，对小白非常友好。

### Windows——Motrix

### 安卓——Aria2app

## Aria2的进阶使用

既然aira2

参考：
