---
title: "Sumatrapdf：全能电子书阅读器"
slug: Sumatrapdf
date: 2022-06-01T17:50:53+08:00
draft: true

---

Sumatra PDF 是一款有着 16 年历史的电子书阅读器，支持 PDF, eBook (epub, mobi), comic book (cbz/cbr), DjVu, XPS, CHM 等格式，甚至支持图片浏览。

PDF, eBook (epub, mobi), comic book (cbz/cbr), DjVu, XPS, CHM, image viewer for Windows.
Small, fast, customizable, free.

[Sumatra PDF 3.4 版本发布，新增命令行、自定义快捷键、mupdf 引擎、网络翻译等功能 - 小众软件](https://www.appinn.com/sumatra-pdf-3-4/)

[Free PDF Reader - Sumatra PDF](https://www.sumatrapdfreader.org/free-pdf-reader)

## 简介

一个简短的介绍

### 相关链接

## 使用教程

一些个比较详细的教程

## 小编吐槽

@stark

stark 的想法

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

