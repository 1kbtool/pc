---
title: "Google身份验证器"
date: 2022-03-01T09:23:35+08:00
draft: false
slug: Googleauthenticator
---

使用Google身份验证器为微软账户、谷歌账户等开启二步验证。

当我们使用国内软件时，通常会绑定手机或者人脸作为二步验证的手段，以避免其他人使用密码直接盗号。但是在使用国外软件时，我们有时并不希望绑定手机号。那么如何增强我们账号的安全性呢？一种很好的方式就是使用二步验证软件。

这是一款非常纯粹的工具，只有保存二步验证码一种功能。其并不需要谷歌框架的支持，所以即使在国产系统也能够很好的运行。

微软也有一款类似的验证工具。但此款验证工具主要的问题是导入和导出并不是很方便。如果你要换手机，就只能解除原手机的绑定。但是这个工具可以和edge联动，用于存储其他软件或者网站的密码啊。


[软件下载](http://tmp.link/f/621d75abd0b80)



参考：
