---
title: "Rustdesk：开源的远程桌面软件"
date: 2022-02-26T14:54:24+08:00
draft: false
slug: Rustdesk
---

远程桌面软件，开箱即用，无需任何配置。您完全掌控数据，不用担心安全问题。您可以使用我们的注册/中继服务器，或者自己设置，亦或者开发您的版本。

支持Windows、macos、Linux全平台。

[软件下载](https://github.com/rustdesk/rustdesk/releases/download/1.1.8/rustdesk-1.1.8-windows_x64-portable.zip)

https://rustdesk.com/

![](2022-02-27-21-59-32.png)

这类软件市面上也有很多了，rustdesk的优势在于开源并且能够自建节点。如果有技术能力可以考虑。

参考：
