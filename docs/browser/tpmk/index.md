---
title: "TamperMonkey：一款跨平台的浏览器插件"
date: 2022-02-16T15:36:22+08:00
draft: false
slug: Tpmk
---

**Tampermonkey**中文名俗称**油猴**，是一款浏览器上的[扩展](https://zh.wikipedia.org/wiki/扩展)[[2\]](https://zh.wikipedia.org/zh-cn/Tampermonkey#cite_note-:0-2)，用户可以通过Tampermonkey添加和使用[脚本](https://zh.wikipedia.org/wiki/脚本)，而脚本是一种可以修改网页[JavaScript](https://zh.wikipedia.org/wiki/JavaScript)的程序。

油猴是一款**用户脚本管理器**（脚本需要单独下载）。它拥有非常丰富的社区资源，成千上万的脚本等待你去使用。各色的脚本提供了诸如**去广告、资源搜索、百度云下载助手、历史价格查询**等非常有用的功能。并且，它支持 Chrome, Microsoft Edge, Safari, Opera Next, 和 Firefox等各大平台，所有脚本和配置在个平台互相通用，它甚至还利用网盘实现了多端存储配置功能。可以说是**真正的All in one神器**。

## 在Windows平台使用油猴

在Windows平台上，我十分推荐使用**Edge浏览器**部署油猴。不为其他，就因为你能打开它的官方应用市场，搜索安装油猴脚本。而无需从其他奇怪的地方下载不明不白的”资源“。

### 1. 从Edge官方市场安装油猴

访问：[Tampermonkey - Microsoft Edge Addons](https://microsoftedge.microsoft.com/addons/detail/tampermonkey/iikmkjmpaadaobahmlepeloendndfphd)安装即可。

为了之后说明的方便，接下来我会对油猴的其他功能做一些介绍。**可以先跳过**，直接转到[脚本获取][1]。


### 2.使用和管理油猴平台

点击油猴的插件图标，如图进入油猴管理的后台：

![image-20211103160824400](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211103160824400.png)

可以启用、停用、删除脚本。双击脚本名可以修改脚本代码。

![image-20211103161435464](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211103161435464.png)

转到实用工具，“云”这一栏中，可以管理你的脚本的云端备份。推荐使用OneDrive。认证后就可以直接导出和导入了。

![image-20211103161702151](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211103161702151.png)

NICE！基本功能介绍完了，接下来就一起获取你的第一个脚本吧。

## 在安卓平台使用油猴

安卓平台支持油猴的浏览器相对较少，并且大多数油猴脚本都是针对电脑设置的，即使能够运行，效果也不好。不推荐在手机端使用。

目前较好的方案是使用[kiwi浏览器](https://soft.kermsite.com/p/kiwi/)

[脚本获取][1]

  [1]: http://blog.kermsite.ml/index.php/archives/113/

参考：
