---
title: "Airtable："
date: 2022-03-18T23:23:42+08:00
draft: true
slug: Airtable
categories: ["Mark"]
tags: ["笔记软件","Mark"]
---

凭着这样的产品初心，Airtable在迭代中逐渐摸准企业办公智能化的走势，找到自己的定位，从在线表格应用向项目管理型平台转型。数据、视图、第三方对接、团队协作——Airtable的核心功能，支撑企业自由搭建出自己需要的系统。数据管理支持多样化字段自定义设置和布局；视图类型多元，满足不同业务场景下对数据和任务管理的便利性需求；第三方对接的选择比较多元，包括Google、邮箱、电商平台、主流网站等；团队协作主要体现在共同编辑管理数据、内部沟通、共同实时编辑文档等。

作者：Priscilla
链接：https://www.zhihu.com/question/33499279/answer/1381567366
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

示例：https://www.wechatsync.com/?utm_source=extension_about

https://airtable.com/shrLSJMnTC2BlmP29/tblApDW0GjKuWiLKU?backgroundColor=gray&viewControls=on

## NocoDB

开源实现

https://blog.csdn.net/wbsu2004/article/details/119959400

## Trello 

任务协作工具

https://www.jianshu.com/p/a873a54de400

https://trello.com/

## list

微软版本（不是很好用）

https://www.ruancan.com/news/microsoft-lists-airtable.html

参考：



## 和airtable交互



[Integrate the Telegram Bot API with the Airtable API - Pipedream](https://pipedream.com/apps/telegram-bot-api/integrations/airtable)
