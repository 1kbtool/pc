---
title: "Typora 激活教程"
description: "Typora 激活教程"
slug: typoraCracker
date: 2022-07-22T12:12:17+08:00
draft: false
tags: ['']
categories: ['']
---

## 简介

>仅供学习交流使用！！！

Markdown 编辑器有很多，但确实 Typora 的颜值最能打。配置好自动上传之后用来写博客（复制博客）还是挺爽的。

之前的 Beta 测试阶段免费，收获了一大批粉丝，之后推出了正式版 Typora 1.0，收费。

![](2022-07-22-12-50-10.png)

以前还可以用 Beta 版本，但是最近打开直接弹窗，挺恶心的。

![](2022-07-22-12-13-30.png)


当然你可以选择购买正版：

![](Typora_qrcode.jpg) 

另一种方式是使用激活工具直接激活。

### 相关链接

- 开源工具（未成功）：[GitHub - fossabot/typoraCracker: A patch and keygen tools for typora.](https://github.com/fossabot/typoraCracker)，工具[备份](https://github.com/Heriec/typoraCracker)

## 使用教程

本来是准备使用开源工具激活的，但是没有成功。这里参考了网站[法海之路 生命不息，折腾不止](https://www.fahai.org/)的一个做法。

首先安装 Typora 本体。

然后 转到[本站分享网盘下载](https://list.1kbtool.com/) 补丁（笔记软件/Typora/winmm.dll）。

关闭 Typora，将这个文件复制到 Typora 安装目录（一般为`C:\Program Files\Typora`。如图所示：

![](2022-07-22-13-10-29.png)

再次打开 Typora 即可。

## 小编吐槽

@royse

迫不得已

## 附录

### Reference

- [法海之路        - 生命不息，折腾不止](https://www.fahai.org/)
- [Typora 激活教程 - 木子欢儿 - 博客园](https://www.cnblogs.com/HGNET/p/15836188.html) 失败


### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

