---
title: "Typoraimg"
description: "Typoraimg"
slug: Typoraimg
date: 2022-07-22T14:55:06+08:00
draft: true
tags: ['']
categories: ['']
---

## 简介

一个简短的介绍

## [Typora+Github+Onedrive：半公开云笔记解决方案](https://blog.kermsite.com/p/cloudnote/)

### Typora用来做笔记，Github用来存图片，Onedrive用来同步

Dec 23, 2021

阅读时长: 4 分钟

## 简介

简单的配置实现半公开式（插入的图片会被公开）的云笔记。通过OneDrive实现同步。对代码格式支持良好。

> 个人使用这套方案其实并不是用来做笔记，而是写博客。平时用Onenote更多一些（毕竟和OneDrive配合更好，对手写和`Unicodemath`的支持也不错，还有良好的笔记整理体系）
>
> 写博客的话，这样生成的笔记文件可以直接上传到hugo和typecho这类博客系统，非常nice。

### 实现原理：

- typora编辑图文，文本部分保存在本地，通过OneDrive自动同步，图片部分自动上传到Github，并套上Jsdeliver加速。
- 在另一台设备访问时，先从Onedrive拉取文本，再从Jsdeliver拉取图片，实现图文的展示。
- 如果要将笔记发布到互联网，只需将文本复制上传即可，知乎、简书、typecho以及hugo等都支持这种方式。

> - markdown：一种简单高效的标记语言。markdown格式的文章可以包含图片、排版、表格、代码块等，却只占用几个kb的空间。许多平台（尤其Github）都支持基于md编辑的图文。
>
> - Typora：一款基于markdown的编辑器，支持自动上传图片。写文章的体验不逊于任何一款笔记软件。
>
>   最近改成收费了，`Beta`版还是一直免费
>
> - Github：代码托管平台，也可以用来托管文件、图片。理论上无限的存储空间。
>
> - Jsdeliver：CDN服务提供商。能够加快Github上文件、图片的加载速度。
>
> - OneDrive：Windows系统自带的云盘，个人有15G免费空间。支持自动同步。

### 演示：



[![演示](https://cdn.jsdelivr.net/gh/kerms0/pics/img/2021-11-08%2013-25-36_4.gif)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/2021-11-08 13-25-36_4.gif)演示



### 搭建成本：

- 零

### 能够实现：

- 多台电脑自动同步笔记。
- 极低的本地硬盘空间、OneDrive存储空间占用。
- 图片插入，图片自动保存云端。
- 便捷的分享：一键发布文章到互联网等。
- 插入代码，代码高亮。

### 不能实现：

- 完全的私有化笔记。——图片会被公开。
- 手写。——md不支持手写。
- 多端同步。——安卓端目前没有找到很好的编辑器。

## 配置Github

登录（注册）[GitHub](https://github.com/)。首先新建一个仓库：



[![创建仓库](image-20211108230005206.png)](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211108230005206.png)创建仓库



仓库名可以自己设置，此处仅作示范，注意选择`Public`，点击`Creat repository`即可。



[![仓库名可以自己设置，此处仅作示范，注意公开](image-20211223162148577.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211223162148577.png)仓库名可以自己设置，此处仅作示范，注意公开



记下你取的仓库名。

生成一个token用于PicGo操作你的仓库，访问：https://github.com/settings/tokens，然后点击`Generate new token`。



[![生成token](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/generate_new_token.png)](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/generate_new_token.png)生成token



把`repo`的勾打上即可。然后翻到页面最底部，点击`Generate token`的绿色按钮生成token。



[![给予权限](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/20180508210435.png)](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/20180508210435.png)给予权限



**注意：**这个token生成后只会显示一次！你要把这个token复制一下备用。



[![记得复制](https://cdn.jsdelivr.net/gh/kerms0/pics/img/copy_token.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/copy_token.png)记得复制



## 下载Typora

注意：**虽然Typora已经转向收费软件，但仍然可以使用开发版**。目前我所使用的就是`0.11.13`：



[![我已出仓，感觉良好](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211212233001600.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211212233001600.png)我已出仓，感觉良好



开发版下载地址：[Typora for windows — beta version release](https://typora.io/windows/dev_release.html)

当然也欢迎大家支持发行版，毕竟也不是特别的贵（非正当途径就算了。。。）：[Typora — a markdown editor, markdown reader.](https://typora.io/)

官方使用教程（英文）：[Typora Support - Typora Support](https://support.typora.io/)

## 配置图片自动上传

安装完成，文件–>偏好设置，按照下图进行选择：

**注意！**要选择“下载或更新”，安装picgo-core才能使用，否则会出现error找不到上传软件



[![设置](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20210826195733347.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20210826195733347.png)设置



将下面代码复制粘贴到你的配置文件中。

```text
{
"picBed": {
    "uploader": "github", 
    "github": {
        "repo": "kerm/test",//填写你自己的仓库名
        "token": "",//上一步生成的token
        "path": "img/", //保存位置，保持不变即可
        "customUrl": "https://cdn.jsdelivr.net/gh/kerm/test",//套一个CDN加速，将最后两部分换成你自己的repo
        "branch": "main" 
    }
},
"picgoPlugins": {} 
} 
```

Copy

保存即可.

此时向typora插入图片即会自动上传到Github，并提取经过加速后的连接放到文本中。

## 其他

### 关于OneDrive

OneDrive无需多做配置，只要将保存时将文件位置选择到OneDrive文件夹中即可，自动同步。文本部分存储在OneDrive中，不会被公开。

注意OneDrive国内直接使用会比较慢，可以考虑加个速。不过传个文本文档应该也还好吧。

### 关于markdown

在typora中的编辑和平时文本编辑是一样的。你可以先熟悉一下导航栏和快捷键。也可以参考官方文档。

> 官方使用教程（英文）：[Typora Support - Typora Support](https://support.typora.io/)

也可参考：[Markdown：语法简介 (kermsite.com)](https://blog.kermsite.com/p/intro-md/)

### 常见错误

如用以上配置（Picgo command line），重复上传同一张图片会报错（文件名冲突）。解决方案是使用Picgo完整版软件。

到这里下载最新版安装：[Releases · Molunerfinn/PicGo (github.com)](https://github.com/Molunerfinn/PicGo/releases)，windows用x64.exe结尾的。

同上在设置图床参数：



[![image-20211223161943913](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211223161943913.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211223161943913.png)image-20211223161943913



然后设置自动改名：



[![image-20210826210025821](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20210826210025821.png)](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20210826210025821.png)image-20210826210025821



即可。

## 附录

### 参考文章

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。

### 相关链接

## 使用教程

一些个比较详细的教程

## 小编吐槽

@stark

stark 的想法

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

