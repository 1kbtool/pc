---
title: "电脑录屏解决方案"
date: 2022-02-16T15:51:23+08:00
draft: false
slug: Srobs
---

前一段时间尝试着录制一些电脑的操作视频，现将使用到的软件附在这里，以备参考：

- OBS——免费而强大的视频录制软件
- Carnac——实时显示键盘操作
- gInk——屏幕书写
- pr——视频编辑（付费软件。这里不做过度介绍，请自行搜索adobe pr）

软件下载地址：

- 官网
  - [Download | OBS (obsproject.com)](https://obsproject.com/download)
  - [Carnac (code52.org)](http://code52.org/carnac/)
  - [geovens/gInk: An easy to use on-screen annotation software inspired by Epic Pen. (github.com)](https://github.com/geovens/gInk)
- 备用下载地址
  - [kermsShareList (kerms-sharelist.herokuapp.com)](https://kerms-sharelist.herokuapp.com/[软件]soft/[视频相关]Video)
  - 请下载压缩文件。

### 简单的使用教程

#### OBS：

OBS的界面对于新手来说的确有些复杂（不过复杂就意味着强大嘛），所以我简单录了一个视频：

[OBS的简单使用_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1Kg411T7X6/)

如果你需要将录制的视频导入pr，这里要注意改一下它的输出格式，否则pr检测不到：
![image-20211112102632307](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211112102632307.png)

![image-20211112102720253](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211112102720253.png)

修改成MP4

#### Carnac

效果：（抱歉我直接使用了项目演示图）

![img](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/screenshot.gif)

直接运行Carnac.exe，**大概率会报错，这时只要单击调试让他自己去修复即可**

![image-20211112104520307](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211112104520307.png)

会直接打开下面界面：

![image-20211112104606600](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211112104606600.png)

左边四个点可以选择展示的位置，下面是简单的调试，最后不要忘了save才能生效。

#### Gink

效果：（抱歉我直接使用了项目演示图）

![img](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/screenshot1.jpg)

点击Gink就可以运行了，运行后不会弹出界面，而会在任务栏里出现一个图标，点击这个图标就可以唤出菜单

![image-20211112104016950](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211112104016950.png)

左边可以切换不同颜色的笔，右边各种功能，中间可以切换回鼠标。

右键单击任务栏里面的图标，还可以进行设置，有中文：
![image-20211112104130015](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211112104130015.png)

#### PR

adobe系列的软件。非常强大的视频剪辑工具。请至官网购买正版。



参考：
